import sys
from pathlib import Path
import ctypes

lib_dir = Path(__file__).resolve().parent
sys.path.insert(0, str(lib_dir))
lib = ctypes.CDLL(str(lib_dir / 'qprogressindicator.so'))


from .qprogressindicator import QProgressIndicator
