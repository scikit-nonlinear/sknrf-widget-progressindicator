## Build-Flow

### Install Dependencies

See requirements.txt

### Release Build

```bash
pip3 install -r requirements.txt
rm -rf build ; mkdir -p build ; cd build
cmake ..
make
sudo make install
```

### Debug Build

```bash
rm -rf build ; mkdir -p build ; cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
sudo make install
```

