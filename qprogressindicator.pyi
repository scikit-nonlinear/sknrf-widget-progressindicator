"""
This file contains the exact signatures for all functions in module
qrangeslider, except for defaults which are replaced by "...".
"""

import PySide6.QtWidgets

import os
import enum
from typing import Any, Callable, Optional, Tuple, Type, Union, Sequence, Dict, List, overload
from shiboken6 import Shiboken


class QProgressIndicator(PySide6.QtWidgets.QWidget):

    def __init__(self, parent: Optional[PySide6.QtWidgets.QWidget] = ...) -> None: ...
